#' Gridded Class
#'
#' Reference class to put source data into a gridded format to be used by an
#' inference module.
Gridded <- R6::R6Class("Gridded", lock_objects = FALSE,
  public = list(

    #' @param inputs An object of class Core.
    #' @param ppmesh An object of class PPmesh.
    #' @param do_parallel Boolean object to indicate if parallelization should be used.
    #'
    #' @seealso Inputs class, PPmesh Class, doParallel Package.
    initialize = function(inputs, ppmesh, do_parallel = FALSE) {

      # Define training grid
      private$generate_ppm_data(inputs, ppmesh, do_parallel)

      # Define prediction grid
      start_forecast <- length(inputs$target_months) - inputs$length_forecast + 1
      private$generate_ppm_pred(inputs, start_forecast = start_forecast)

    }

  ),

  private = list(

    # Put inputs data in a mesh grid to be passed as training set of a point process model.
    generate_ppm_data = function(inputs, ppmesh, do_parallel) {

      "%>%" <- magrittr::"%>%"

      # Cases month and location (space and time)
      start_month <- lubridate::month(inputs$start_date)
      index_month <- c(start_month:12, 1:(start_month - 1))[1:12]

      cases_year_calendar <- lubridate::year(inputs$cases$local$Date)
      cases_year_calendar <- (cases_year_calendar - min(cases_year_calendar)) * 12
      cases_month_calendar <- lubridate::month(inputs$cases$local$Date) # calendar months: 1 == January
      cases_month_calendar <- cases_month_calendar + cases_year_calendar

      cases_month_sequence <- cases_month_calendar - start_month + 1
      cases_location <- as.data.frame(inputs$cases$local[, c("Lng", "Lat")])
      names(cases_location) <- tolower(names(cases_location))

      # Data frame with aggregate cases per pixel and month
      month_batches <- split(cases_location, cases_month_sequence)
      aggr_pixels <- do.call(rbind,
                             lapply(month_batches,
                                    private$aggregate_cases_batch,
                                    inputs$target_layer))
      aggr_pixels$pop_offset <- aggr_pixels[, c("lng", "lat")] %>%
        raster::extract(inputs$population_tsk, .)
      aggr_pixels$month_sequence <- sapply(strsplit(row.names(aggr_pixels), "[.]"),
                                           FUN = function(x) as.numeric(x[[1]][1]))

      # Integration points
      num_months <- ppmesh$temporal_mesh$n
      integ_points <- sp::SpatialPoints(ppmesh$integ_points(), proj4string = inputs$boundary@proj4string)
      inner_mask <- !is.na(sp::over(integ_points, inputs$boundary)[ ,1])
      inner_points <- integ_points[inner_mask, ]
      zero_pixels <- data.frame("lng" = inner_points$lng,
                                "lat" = inner_points$lat,
                                "pixel" = raster::cellFromXY(inputs$target_layer, inner_points),
                                "points" = 0,
                                "pop_offset" = ppmesh$tiled_variable(inputs$population_tsk, do_parallel = do_parallel)[inner_mask]
                                ) %>%
        dplyr::filter(!is.na(pixel)) %>%
        dplyr::filter(pop_offset > 0) %>%
        (function(x) x[rep(1:nrow(x), ppmesh$temporal_mesh$n), ])
      zero_pixels$month_sequence <- sort(rep(1:ppmesh$temporal_mesh$n,
                                             nrow(zero_pixels)/ppmesh$temporal_mesh$n))

      # Joint data frame: aggr and zero pixels
      ppm_data <- rbind(aggr_pixels, zero_pixels)
      ppm_data$case <- ifelse(ppm_data$points > 0, 1, 0) # PPM Outcome: 1 at cells with cases and 0 otherwise
      ppm_data$regression_weights <- ppm_data$points %>% # PPM regression weights
        sapply(., function(x) ifelse(x == 0, 1, x)) # Change any 0's to 1's
      # NOTE uncomment %>% at the end of line and the line below
      ppm_data$pop_offset <- ppm_data$pop_offset %>%
        (function(x) x/ppm_data$regression_weights) # Divide the offset by the number of cases in each cell


      train_xy <- ppm_data[, c("lng", "lat")]
      train_z <- inputs$target_months[ppm_data$month_sequence]
      data_extr <- lapply(inputs$cov_names, FUN = inputs$extract_variable, xy = train_xy, z = train_z)
      for(i in seq(along.with = inputs$cov_names)) {
        ppm_data[[inputs$cov_names[i]]] <- data_extr[[i]]
      }

      # Remove points with elevation == NA
      self$ppm_data <- ppm_data[!is.na(ppm_data$elevation), ]

    },

    # Aggregate number of cases by cell and set pixels centroid as coordinates
    aggregate_cases_batch = function(cases_batch, target_layer) {

      cases_batch$pixel <- raster::cellFromXY(target_layer, cases_batch)
      cell_counts <- raster::aggregate(cases_batch$pixel, by = list(cases_batch$pixel), FUN = length)
      counts_batch <- cases_batch[match(cell_counts$Group.1, cases_batch$pixel),]
      counts_batch[,c("lng", "lat")] <- sp::coordinates(target_layer)[counts_batch$pixel,]
      counts_batch$points <- cell_counts$x # These are the aggregate counts
      return(counts_batch)

    },

    # Define a grid of locations and covariates associated to be used for making predictions.
    generate_ppm_pred = function(inputs, start_forecast) {

      num_knots <- length(inputs$target_months)
      seq_knots <- 1:num_knots
      self$target_layer <- inputs$target_layer
      self$target_mask <- !is.na(inputs$target_layer[]) & !is.na(inputs$get_variable_data("elevation")[]) #TODO why elevation?
      num_locs <- sum(self$target_mask)
      cell_locs <- (1:length(self$target_mask))[self$target_mask]
      seq_locs <- 1:num_locs
      pred_xy <- raster::xyFromCell(inputs$target_layer, cell_locs)[rep(seq_locs, num_knots), ]
      pred_ms <- sort(rep(seq_knots, num_locs))
      pred_z <- inputs$target_months[pred_ms]
      quad_w <- rep(inputs$population_tsk[cell_locs], num_knots)
      tsk <- rep(inputs$probability_tsk[cell_locs], num_knots)
      ppm_pred <- data.frame("lng" = pred_xy[, 1],
                             "lat" = pred_xy[, 2],
                             "pixel" = rep(cell_locs, num_knots),
                             "forecast" = FALSE,
                             "month_sequence" = pred_ms,
                             "pop_offset" = quad_w,
                             "probability_tsk" = tsk)
      ppm_pred$forecast[ppm_pred$month_sequence >= start_forecast] <- TRUE

      pred_extr <- lapply(inputs$cov_names, FUN = inputs$extract_variable, xy = pred_xy, z = pred_z)

      for (i in seq(along.with = inputs$cov_names)) {
        ppm_pred[[inputs$cov_names[i]]] <- pred_extr[[i]]
      }

      self$ppm_pred <- ppm_pred

    }

  )
)
