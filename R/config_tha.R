#' ConfigTHA
#'
#' Configuration parameters and routines for Thailand.
ConfigTHA <- R6::R6Class("ConfigTHA", inherit = Core, lock_objects = FALSE,
  public = list(

    #' @param given_end_date End date of the analysis. NULL value (defaults to
    #' current month) or character string in the format yyyy-mm-dd.
    #' @param start_date Initial date of the analysis. NULL value (defaults to
    #' 12 months prior the end date) or character string in the format
    #' yyyy-mm-dd.
    #' @param length_forecast Number of months to forecast after the end date.
    #' Numeric integer object.
    #' @param force_download Boolean object (defaults to FALSE) to force
    #' downloading the input data from their remote sources. This parameter is
    #' ignored if skip_download is set to TRUE.
    #' @param skip_download Boolean object (defaults to FALSE) to skip
    #' downloading  the input data from their remote sources even if they are
    #' not stored locally.
    #'
    #' @note This class inherits from Core class.
    #' @seealso Core class.
    initialize = function(given_end_date = NULL, start_date = NULL,
                          length_forecast = 1, force_download = FALSE,
                          skip_download = FALSE) {

      # Define country specific parameters
      self$bounding_box = c(97.345192, 5.616042, 105.63913, 20.46321)
      self$density = 1/20 #TODO check where this is used

      # Call Core class
      super$initialize(given_end_date = given_end_date, start_date = start_date,
                          length_forecast = length_forecast, force_download = force_download,
                          skip_download = skip_download)

    }

  ),

  private = list(

    # Define country-specific metadata
    set_metadata = function() {

      metadata_list <- list(

        # case_locations
        data.frame(name =       "case_locations",
                   source =     "gs",
                   url =        "cases_buffered.csv",
                   destfile =   "cases_buffered.csv",
                   type =       "csv",
                   covariate =  FALSE,
                   linear_fit = NA,
                   monthly =    FALSE,
                   lag =        NA),
        # population
        data.frame(name =       "population",
                   source =     "gs",
                   url =        "tha_pop.tif",
                   destfile =   "tha_pop.tif",
                   type =       "raster",
                   covariate =  FALSE,
                   linear_fit = NA,
                   monthly =    FALSE,
                   lag =        NA),
        # treatment_seeking
        data.frame(name =       "treatment_seeking",
                   source =     "gs",
                   url =        "ProbSeekTreatTHA.tif",
                   destfile =   "ProbSeekTreatTHA.tif",
                   type =       "raster",
                   covariate =  FALSE,
                   linear_fit = NA,
                   monthly =    FALSE,
                   lag =        NA),
        # distance_to_rivers
        #data.frame(name =       "distance_to_rivers",
        #           source =     "gs",
        #           url =        "DistToRivers.tif",
        #           destfile =   "DistToRivers.tif",
        #           type =       "raster",
        #           covariate =  TRUE,
        #           linear_fit = TRUE,
        #           monthly =    FALSE,
        #           lag =        NA),
        # elevation
        data.frame(name =       "elevation",
                   source =     "gs",
                   url =        "elev_tha.tif",
                   destfile =   "elev_tha.tif",
                   type =       "raster",
                   covariate =  TRUE,
                   linear_fit = NA,
                   monthly =    FALSE,
                   lag =        NA),
        # boundary
        data.frame(name =       "boundary",
                   source =     "getData",
                   url =        "GADM-0",
                   destfile =   NA,
                   type =       "shapefile",
                   covariate =  FALSE,
                   linear_fit = NA,
                   monthly =    FALSE,
                   lag =        NA),
        # ndwi
        data.frame(name =       "ndwi",
                   source =     "ee",
                   url =        NA,
                   destfile =   "ndwi",
                   type =       "raster",
                   covariate =  TRUE,
                   linear_fit = NA,
                   monthly =    TRUE,
                   lag =        0),
        # lst
        data.frame(name =       "lst",
                   source =     "ee",
                   url =        NA,
                   destfile =   "lst",
                   type =       "raster",
                   covariate =  TRUE,
                   linear_fit = NA,
                   monthly =    TRUE,
                   lag =        0),
        # evi
        data.frame(name =       "evi",
                   source =     "ee",
                   url =        NA,
                   destfile =   "evi",
                   type =       "raster",
                   covariate =  TRUE,
                   linear_fit = NA,
                   monthly =    TRUE,
                   lag =        0),
        # precipitation
        data.frame(name =       "precipitation",
                   source =     "ee",
                   url =        NA,
                   destfile =   "precip",
                   type =       "raster",
                   covariate =  TRUE,
                   linear_fit = NA,
                   monthly =    TRUE,
                   lag =        0),
        # imported cases
        data.frame(name =       "imported_case_density_stack",
                   source =     NA,
                   url =        NA,
                   destfile =   NA,
                   type =       NA,
                   covariate =  TRUE,
                   linear_fit = NA,
                   monthly =    TRUE,
                   lag =        1)

      )

      self$metadata <- do.call(rbind, metadata_list)

    },

    # Case data loading and cleaning
    #load_and_clean = function(cases_filename) {
    custom_clean = function(cases) {

      # Load
      #cases <- dplyr::tbl_df(read.csv(file.path(self$input_files_path, cases_filename),
      #                                stringsAsFactors = FALSE))

      # Clean
      ##tidied_cases <- private$tidy_data(cases) # This method is in the super class
      #"%>%" <- magrittr::"%>%"

      ## Separate local from imported cases
      #lag_imp <- self$metadata$lag[self$metadata$name == "imported_case_density_stack"]

      #start_date_loc <- self$start_date
      #start_date_imp <- self$start_date
      #lubridate::month(start_date_imp) <- lubridate::month(start_date_imp) - lag_imp

      #cases_to_date_loc <- self$cases_to_date
      #cases_to_date_imp <- self$end_forecast - lag_imp

      #output <- list(
      #  local = tidied_cases %>% dplyr::filter(Review == "local") %>%
      #    dplyr::filter(Date >= start_date_loc & Date <= cases_to_date_loc),
      #  imported = tidied_cases %>% dplyr::filter(Review == "imported") %>%
      #    dplyr::filter(Date >= start_date_imp & Date <= cases_to_date_imp)
      #)

      return(cases)

    }

  )
)
