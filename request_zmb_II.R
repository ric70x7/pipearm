devtools::document()

load("countries/SWZ/output/years_14_16.RData")

# Raster aggregates
swz_mask[r_mask] <- is.na(m1$raster_cases[[1]][])
   
cases_year1 <- m1$raster_cases[[1]]
cases_year1[swz_raster] <- sum(m1$raster_cases[[1:12]])[r_mask]
names(cases_year1) <- "cases_year1"
writeRaster(cases_year1, filename = "countries/SWZ/output/years_14_16/cases_year1.tif", format = "GTiff")

reported_year1 <- m1$raster_reported[[1]]
reported_year1[swz_raster] <- sum(m1$raster_reported[[1:12]])[r_mask]
names(reported_year1) <- "reported_year1"
writeRaster(reported_year1, filename = "countries/SWZ/output/years_14_16/reported_year1.tif", format = "GTiff")

rate_year1 <- m1$raster_rate[[1]]
rate_year1[swz_raster] <- mean(m1$raster_rate[[1:12]])[r_mask]
names(rate_year1) <- "rate_year1"
writeRaster(rate_year1, filename = "countries/SWZ/output/years_14_16/rate_year1.tif", format = "GTiff")


cases_year2 <- m1$raster_cases[[1]]
cases_year2[swz_raster] <- sum(m1$raster_cases[[13:24]])[r_mask]
names(cases_year2) <- "cases_year2"
writeRaster(cases_year2, filename = "countries/SWZ/output/years_14_16/cases_year2.tif", format = "GTiff")

reported_year2 <- m1$raster_reported[[1]]
reported_year2[swz_raster] <- sum(m1$raster_reported[[13:24]])[r_mask]
names(reported_year2) <- "reported_year2"
writeRaster(reported_year2, filename = "countries/SWZ/output/years_14_16/reported_year2.tif", format = "GTiff")

rate_year2 <- m1$raster_rate[[1]]
rate_year2[swz_raster] <- mean(m1$raster_rate[[13:24]])[r_mask]
names(rate_year2) <- "rate_year2"
writeRaster(rate_year2, filename = "countries/SWZ/output/years_14_16/rate_year2.tif", format = "GTiff")


# Covariates 

for (name_i in m1$cov_names) {
    aux <- config$get_variable_data(name_i)
    if (dim(aux[3] > 1) {
      aux_mask <- aux[[1]]
      aux[swz_raster] <- mean(aux[[1:12]])[r_mask]
      writeRaster(aux, filename = paste0("countries/SWZ/output/years_14_16/", name_i, "_year1.tif"), format = "GTiff")
      aux[swz_raster] <- mean(aux[[13:24]])[r_mask]
      writeRaster(aux, filename = paste0("countries/SWZ/output/years_14_16/", name_i, "_year2.tif"), format = "GTiff")
    } else {
      writeRaster(aux, filename = paste0("countries/SWZ/output/years_14_16/", name_i, "_year1.tif"), format = "GTiff")
      writeRaster(aux, filename = paste0("countries/SWZ/output/years_14_16/", name_i, "_year2.tif"), format = "GTiff")
    }
}









#load("countries/SWZ/output/years_14_15/years_14_15.RData")

#for(i in unique(m1$ppm_data$month_sequence)) {
#  print(c(sum(subset(m1$ppm_data, month_sequence == i)$case), sum(m1$ppm_pred, month, na.rm = TRUE)))
#}

#my1 <- subset(m1$ppm_pred, month_sequence <= 12)
#my2 <- subset(m1$ppm_pred, month_sequence > 12)

#rm(config)
#rm(gridded)
#rm(lp1)
#rm(m1)
#rm(ppmesh)

#save(my1, file = "countries/SWZ/output/my1.RData")
#load("countries/SWZ/output/my1.RData")

#my1_cases <- aggregate(cases ~ lng + lat, FUN = sum, data = my1)
#save(my1_cases, file = "countries/SWZ/output/my1_cases.RData")

#my1_reported <- aggregate(cases ~ lng + lat, FUN = sum, data = my1)
#my1_rate <- aggregate(cases ~ lng + lat, FUN = mean, data = my1)

#my2_cases <- aggregate(cases ~ lng + lat, FUN = sum, data = my1)
#my2_reported <- aggregate(cases ~ lng + lat, FUN = sum, data = my1)
#my2_rate <- aggregate(cases ~ lng + lat, FUN = mean, data = my1)


#dim(my1_cases)

#load("countries/SWZ/output/my1_cases.RData")
#library(ggplot2)
#library(ggthemes)

#cases_14 <- ggplot(my1_cases, aes(lng, lat)) +
#            geom_raster(aes(fill = cases)) +
#            scale_fill_distiller(palette = "Spectral") +
#            ggtitle("Cases: Jul 2014 - Jun 2015") +
#            theme(panel.background = element_blank(),
#                  plot.title=element_text(size=24),
#                  axis.title.x=element_blank(),
#                  axis.title.y=element_blank(),
#                  axis.text.x=element_blank(),
#                  axis.text.y=element_blank(),
#                  axis.ticks=element_blank(),
#                  legend.title=element_text(size=18),
#                  legend.text=element_text(size=16)) +
#            coord_fixed()

#ggsave(plot = cases_14, filename = "countries//SWZ/output/years_14_16/cases_14.png")
