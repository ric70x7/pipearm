# Unit tests for util.R

# Test for snake_case method
testthat::test_that("snake_case", {
  string_tested <- "HELLO.__worlD :-)"
  # Test no caps
  testthat::expect_false(grepl("[A-Z]", snake_case(string_tested)))
  # Test no dots
  testthat::expect_false(grepl("\\.", snake_case(string_tested)))
  # Test no multiple underscores
  testthat::expect_false(grepl("_+_", snake_case(string_tested)))
})
